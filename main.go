package main

func main() {
	tiles := []Tile{
		NewTile(51, 7),
		NewTile(51, 8),
		NewTile(51, 9),
		NewTile(50, 7),
		NewTile(50, 8),
		NewTile(50, 9),
		NewTile(49, 7),
		NewTile(49, 8),
		NewTile(49, 9),
	}

	for _, tile := range tiles {
		tile.Download()
	}
}

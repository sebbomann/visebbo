package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"strings"
)

type Tile struct {
	lat int
	lon int
}

func NewTile(lat int, lon int) Tile {
	return Tile{
		lat: lat,
		lon: lon,
	}
}

func (tile Tile) Download() {
	url := buildURL(tile.lat, tile.lon)
	filename := buildFilename(tile.lat, tile.lon)
	filepath := fmt.Sprintf("tiles/%s", filename)

	log.WithFields(log.Fields{
		"lat":  tile.lat,
		"lon":  tile.lon,
		"src":  url,
		"dest": filepath,
	}).Info("Downloading tile")

	err := downloadURL(url, filepath)
	if err != nil {
		log.WithFields(log.Fields{
			"lat":  tile.lat,
			"lon":  tile.lon,
			"src":  url,
			"dest": filepath,
		}).Errorf("Problem downloading tile: %s", err)
	}
}

func buildFilename(lat int, lon int) string {
	var filename strings.Builder

	if lat >= 0 {
		filename.WriteString(fmt.Sprintf("N%02d", lat))
	} else {
		filename.WriteString(fmt.Sprintf("S%02d", lat))
	}

	if lon >= 0 {
		filename.WriteString(fmt.Sprintf("E%03d", lon))
	} else {
		filename.WriteString(fmt.Sprintf("W%03d", lon))
	}
	filename.WriteString(".hgt")

	return filename.String()
}

func buildURL(lat int, lon int) string {
	var url strings.Builder

	url.WriteString("https://cloud.sdsc.edu/v1/AUTH_opentopography/Raster/SRTM_GL1/SRTM_GL1_srtm/")

	if lat >= 0 {
		url.WriteString("North/")
		if lat < 30 {
			url.WriteString("North_0_29/")
		} else {
			url.WriteString("North_30_60/")
		}
	} else {
		url.WriteString("South/")
	}

	url.WriteString(buildFilename(lat, lon))

	return url.String()
}

func downloadURL(url string, filepath string) (err error) {
	file, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer file.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
